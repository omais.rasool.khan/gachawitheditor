#pragma once
#include "json.hpp"
#include <string>
class card
{
public:
	card();
	~card();

	void initializeCard(json::JSON cardData);

	std::string getCardName();
	std::string getCardType();
	int getCartScore();
private:
	std::string cardName;
	std::string cardType;
	int cardScore;
};

