#include "deck.h"
#include <algorithm>
#include <random>



deck::deck() 
{
	std::cout << "Deck Created" << std::endl;
}

deck::~deck() 
{
	for (std::vector<card*>::iterator cardObj = cardSet.begin(); cardObj != cardSet.end(); ++cardObj)
	{
		delete (*cardObj);
	}
	std::cout << "deck destroyed" << std::endl;
}

void deck::initializeDeck(json::JSON cardsArray) 
{
	std::cout << "Deck initialize called to call card initialize" << std::endl;

	for (auto& cardCharacter : cardsArray.ArrayRange())
	{
		std::cout << cardCharacter.ToString() << std::endl;
		card* newCard = new card;
		newCard->initializeCard(cardCharacter);
		cardSet.push_back(newCard);
	}
	std::shuffle(cardSet.begin(), cardSet.end(), std::default_random_engine(rand()));
	std::cout << "Initial deck size >>>> " << cardSet.size() << std::endl;
	while (cardSet.size() > 15)
	{
		delete cardSet.back();
		cardSet.pop_back();
	}
}

void deck::InitializeWeakDeck(json::JSON cardsArray) 
{
	std::cout << "Deck initialize called to call card initialize" << std::endl;

	for (auto& cardCharacter : cardsArray.ArrayRange())
	{
		if (cardCharacter["score"].ToInt() < 3) 
		{
			card* newCard = new card;
			newCard->initializeCard(cardCharacter);
			cardSet.push_back(newCard);
		}
	}
	std::shuffle(cardSet.begin(), cardSet.end(), std::default_random_engine(rand()));
	std::cout << "Initial deck size >>>> " << cardSet.size() << std::endl;
	while (cardSet.size() > 15)
	{
		delete cardSet.back();
		cardSet.pop_back();
	}
}

void deck::InitializeMediumDeck(json::JSON cardsArray)
{
	std::cout << "Deck initialize called to call card initialize" << std::endl;

	for (auto& cardCharacter : cardsArray.ArrayRange())
	{
		if (cardCharacter["score"].ToInt() >= 3 && cardCharacter["score"].ToInt() < 7)
		{
			card* newCard = new card;
			newCard->initializeCard(cardCharacter);
			cardSet.push_back(newCard);
		}
	}
	std::shuffle(cardSet.begin(), cardSet.end(), std::default_random_engine(rand()));
	std::cout << "Initial deck size >>>> " << cardSet.size() << std::endl;
	while (cardSet.size() > 15)
	{
		delete cardSet.back();
		cardSet.pop_back();
	}
}


void deck::InitializestrongDeck(json::JSON cardsArray)
{
	std::cout << "Deck initialize called to call card initialize" << std::endl;

	for (auto& cardCharacter : cardsArray.ArrayRange())
	{
		if (cardCharacter["score"].ToInt() >= 5)
		{
			card* newCard = new card;
			newCard->initializeCard(cardCharacter);
			cardSet.push_back(newCard);
		}
	}
	std::shuffle(cardSet.begin(), cardSet.end(), std::default_random_engine(rand()));
	std::cout << "Initial deck size >>>> " << cardSet.size() << std::endl;
	while (cardSet.size() > 15)
	{
		delete cardSet.back();
		cardSet.pop_back();
	}
}




card* deck::dealCard() {
	//card* newCard = new card;
	card* newCard;
	newCard = cardSet.back();
	cardSet.back() = nullptr;
	cardSet.pop_back();
	return newCard;
}

void deck::shuffle() 
{
	std::shuffle(cardSet.begin(), cardSet.end(), std::default_random_engine());
}

std::vector<card*> deck::getCardSet() 
{
	return cardSet;
}