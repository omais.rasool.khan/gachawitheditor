#pragma once

#include "SFML/System.hpp"
#include "SFML/Graphics.hpp"
#include "gameManager.h"

class Draw
{
private:

	gameManager* playerManager;
	sf::RenderWindow* window;
	sf::Texture &cardFrame;
	std::vector<sf::Sprite*> &cardCharactersSprite;
	std::vector<sf::Sprite*> &cardFrameSprite;
	std::vector<sf::Texture*> &cardTexture;
	std::vector<std::vector<float>> &cardPositions;

public:
	Draw(gameManager* playerManager, sf::RenderWindow* window, sf::Texture cardFrame,
		std::vector<sf::Sprite*> cardCharactersSprite,
		std::vector<sf::Sprite*> cardFrameSprite, std::vector<sf::Texture*> cardTexture,
		std::vector<std::vector<float>> cardPositions);
	~Draw();

	void draw();
};

