#pragma once
#include "json.hpp"
#include "card.h"
#include <vector>
class deck
{
public:
	
	deck();
	~deck();

	void initializeDeck(json::JSON cardsArray);
	void InitializeWeakDeck(json::JSON cardsArray);
	void InitializeMediumDeck(json::JSON cardsArray);
	void InitializestrongDeck(json::JSON cardsArray);
	void shuffle();
	card* dealCard();
	std::vector<card*> getCardSet();
private:

	std::vector<card*> cardSet;

	std::vector<card*> hand;
};

