﻿namespace Gacha_projEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ImagesBox = new System.Windows.Forms.ComboBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.pictureLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cardClassBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cardPowerText = new System.Windows.Forms.TextBox();
            this.cardsListBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.EnterCardNameBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.CreateCardComboBox = new System.Windows.Forms.ComboBox();
            this.CreateCardClassBox = new System.Windows.Forms.ComboBox();
            this.CreateCardSetPowerBox = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.AddCardButton = new System.Windows.Forms.Button();
            this.LoadCardButton = new System.Windows.Forms.Button();
            this.EditCardButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.currentCardPic = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.currentClassBox = new System.Windows.Forms.TextBox();
            this.currentPowerBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.setCardNameBox = new System.Windows.Forms.TextBox();
            this.saveButton1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(393, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(480, 299);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // ImagesBox
            // 
            this.ImagesBox.FormattingEnabled = true;
            this.ImagesBox.Location = new System.Drawing.Point(20, 190);
            this.ImagesBox.Name = "ImagesBox";
            this.ImagesBox.Size = new System.Drawing.Size(121, 24);
            this.ImagesBox.TabIndex = 3;
            this.ImagesBox.SelectedIndexChanged += new System.EventHandler(this.ImagesBox_SelectedIndexChanged);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // pictureLabel
            // 
            this.pictureLabel.AutoSize = true;
            this.pictureLabel.Location = new System.Drawing.Point(20, 161);
            this.pictureLabel.Name = "pictureLabel";
            this.pictureLabel.Size = new System.Drawing.Size(95, 17);
            this.pictureLabel.TabIndex = 4;
            this.pictureLabel.Text = "Select Picture";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 232);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Set Class";
            // 
            // cardClassBox
            // 
            this.cardClassBox.FormattingEnabled = true;
            this.cardClassBox.Location = new System.Drawing.Point(20, 266);
            this.cardClassBox.Name = "cardClassBox";
            this.cardClassBox.Size = new System.Drawing.Size(121, 24);
            this.cardClassBox.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 312);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "Set Power";
            // 
            // cardPowerText
            // 
            this.cardPowerText.Location = new System.Drawing.Point(20, 348);
            this.cardPowerText.Name = "cardPowerText";
            this.cardPowerText.Size = new System.Drawing.Size(100, 22);
            this.cardPowerText.TabIndex = 9;
            // 
            // cardsListBox
            // 
            this.cardsListBox.FormattingEnabled = true;
            this.cardsListBox.Location = new System.Drawing.Point(198, 117);
            this.cardsListBox.Name = "cardsListBox";
            this.cardsListBox.Size = new System.Drawing.Size(121, 24);
            this.cardsListBox.TabIndex = 10;
            this.cardsListBox.SelectedIndexChanged += new System.EventHandler(this.cardsListBox_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(195, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Select card";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(957, 498);
            this.tabControl1.TabIndex = 14;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.AddCardButton);
            this.tabPage1.Controls.Add(this.pictureBox2);
            this.tabPage1.Controls.Add(this.CreateCardSetPowerBox);
            this.tabPage1.Controls.Add(this.CreateCardClassBox);
            this.tabPage1.Controls.Add(this.CreateCardComboBox);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.EnterCardNameBox);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(949, 469);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Create New Card";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.saveButton1);
            this.tabPage2.Controls.Add(this.setCardNameBox);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.currentPowerBox);
            this.tabPage2.Controls.Add(this.currentClassBox);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.currentCardPic);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.EditCardButton);
            this.tabPage2.Controls.Add(this.LoadCardButton);
            this.tabPage2.Controls.Add(this.cardsListBox);
            this.tabPage2.Controls.Add(this.pictureBox1);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.pictureLabel);
            this.tabPage2.Controls.Add(this.cardPowerText);
            this.tabPage2.Controls.Add(this.ImagesBox);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.cardClassBox);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(949, 469);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Edit Cards";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 17);
            this.label4.TabIndex = 12;
            this.label4.Text = "Set Card Name";
            // 
            // EnterCardNameBox
            // 
            this.EnterCardNameBox.Location = new System.Drawing.Point(9, 80);
            this.EnterCardNameBox.Name = "EnterCardNameBox";
            this.EnterCardNameBox.Size = new System.Drawing.Size(100, 22);
            this.EnterCardNameBox.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 17);
            this.label5.TabIndex = 14;
            this.label5.Text = "Select Picture";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 175);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "Set Class";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 235);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 17);
            this.label7.TabIndex = 16;
            this.label7.Text = "Set Power";
            // 
            // CreateCardComboBox
            // 
            this.CreateCardComboBox.FormattingEnabled = true;
            this.CreateCardComboBox.Location = new System.Drawing.Point(6, 136);
            this.CreateCardComboBox.Name = "CreateCardComboBox";
            this.CreateCardComboBox.Size = new System.Drawing.Size(121, 24);
            this.CreateCardComboBox.TabIndex = 17;
            this.CreateCardComboBox.SelectedIndexChanged += new System.EventHandler(this.CreateCardComboBox_SelectedIndexChanged);
            // 
            // CreateCardClassBox
            // 
            this.CreateCardClassBox.FormattingEnabled = true;
            this.CreateCardClassBox.Location = new System.Drawing.Point(6, 198);
            this.CreateCardClassBox.Name = "CreateCardClassBox";
            this.CreateCardClassBox.Size = new System.Drawing.Size(121, 24);
            this.CreateCardClassBox.TabIndex = 18;
            // 
            // CreateCardSetPowerBox
            // 
            this.CreateCardSetPowerBox.Location = new System.Drawing.Point(6, 255);
            this.CreateCardSetPowerBox.Name = "CreateCardSetPowerBox";
            this.CreateCardSetPowerBox.Size = new System.Drawing.Size(100, 22);
            this.CreateCardSetPowerBox.TabIndex = 19;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(379, 80);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(480, 299);
            this.pictureBox2.TabIndex = 20;
            this.pictureBox2.TabStop = false;
            // 
            // AddCardButton
            // 
            this.AddCardButton.Location = new System.Drawing.Point(9, 324);
            this.AddCardButton.Name = "AddCardButton";
            this.AddCardButton.Size = new System.Drawing.Size(118, 55);
            this.AddCardButton.TabIndex = 21;
            this.AddCardButton.Text = "Add Card to Deck";
            this.AddCardButton.UseVisualStyleBackColor = true;
            this.AddCardButton.Click += new System.EventHandler(this.AddCardButton_Click);
            // 
            // LoadCardButton
            // 
            this.LoadCardButton.Location = new System.Drawing.Point(9, 22);
            this.LoadCardButton.Name = "LoadCardButton";
            this.LoadCardButton.Size = new System.Drawing.Size(118, 55);
            this.LoadCardButton.TabIndex = 22;
            this.LoadCardButton.Text = "Load Cards from Deck";
            this.LoadCardButton.UseVisualStyleBackColor = true;
            this.LoadCardButton.Click += new System.EventHandler(this.LoadCardButton_Click);
            // 
            // EditCardButton
            // 
            this.EditCardButton.Location = new System.Drawing.Point(24, 398);
            this.EditCardButton.Name = "EditCardButton";
            this.EditCardButton.Size = new System.Drawing.Size(91, 45);
            this.EditCardButton.TabIndex = 23;
            this.EditCardButton.Text = "Edit Selected Card";
            this.EditCardButton.UseVisualStyleBackColor = true;
            this.EditCardButton.Click += new System.EventHandler(this.EditCardButton_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(195, 161);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 17);
            this.label8.TabIndex = 24;
            this.label8.Text = "Current Picture";
            // 
            // currentCardPic
            // 
            this.currentCardPic.Location = new System.Drawing.Point(198, 190);
            this.currentCardPic.Name = "currentCardPic";
            this.currentCardPic.Size = new System.Drawing.Size(100, 22);
            this.currentCardPic.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(195, 232);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 17);
            this.label9.TabIndex = 26;
            this.label9.Text = "Current Class";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // currentClassBox
            // 
            this.currentClassBox.Location = new System.Drawing.Point(198, 266);
            this.currentClassBox.Name = "currentClassBox";
            this.currentClassBox.Size = new System.Drawing.Size(100, 22);
            this.currentClassBox.TabIndex = 27;
            // 
            // currentPowerBox
            // 
            this.currentPowerBox.Location = new System.Drawing.Point(198, 348);
            this.currentPowerBox.Name = "currentPowerBox";
            this.currentPowerBox.Size = new System.Drawing.Size(100, 22);
            this.currentPowerBox.TabIndex = 28;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(195, 312);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 17);
            this.label10.TabIndex = 29;
            this.label10.Text = "Current Power";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 97);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(104, 17);
            this.label11.TabIndex = 30;
            this.label11.Text = "Set Card Name";
            // 
            // setCardNameBox
            // 
            this.setCardNameBox.Location = new System.Drawing.Point(20, 117);
            this.setCardNameBox.Name = "setCardNameBox";
            this.setCardNameBox.Size = new System.Drawing.Size(100, 22);
            this.setCardNameBox.TabIndex = 31;
            this.setCardNameBox.TextChanged += new System.EventHandler(this.setCardNameBox_TextChanged);
            // 
            // saveButton1
            // 
            this.saveButton1.Location = new System.Drawing.Point(198, 22);
            this.saveButton1.Name = "saveButton1";
            this.saveButton1.Size = new System.Drawing.Size(75, 23);
            this.saveButton1.TabIndex = 32;
            this.saveButton1.Text = "Save Deck";
            this.saveButton1.UseVisualStyleBackColor = true;
            this.saveButton1.Click += new System.EventHandler(this.saveButton1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1046, 522);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox ImagesBox;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label pictureLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cardClassBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox cardPowerText;
        private System.Windows.Forms.ComboBox cardsListBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox CreateCardSetPowerBox;
        private System.Windows.Forms.ComboBox CreateCardClassBox;
        private System.Windows.Forms.ComboBox CreateCardComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox EnterCardNameBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button AddCardButton;
        private System.Windows.Forms.Button LoadCardButton;
        private System.Windows.Forms.Button EditCardButton;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox currentCardPic;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox currentPowerBox;
        private System.Windows.Forms.TextBox currentClassBox;
        private System.Windows.Forms.TextBox setCardNameBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button saveButton1;
    }
}

