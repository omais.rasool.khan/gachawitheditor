﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Gacha_projEditor
{
    public partial class Form1 : Form
    {
        private List<card> myCards;
        string path = AppDomain.CurrentDomain.BaseDirectory; //set directory where JSon files are
        string imagePath = @"characters\"; //set path for images folder
        string filePath = "";
        List<string> imageListPaths = new List<string>(); //list to store image file paths
        List<string> imageNames = new List<string>(); //list to store image file names

        public Form1()
        {
            InitializeComponent();
            myCards = new List<card>();

            LoadImages();
            addItemsToCardClassBox();
            addItemsToCardClassBox2();
        }

        //code for displaying image Taken from https://docs.microsoft.com/en-us/dotnet/api/system.windows.forms.picturebox?view=netcore-3.1
        private Bitmap MyImage;
        private Bitmap MyImage2;
        public void ShowMyImage(String fileToDisplay, int xSize, int ySize)
        {
            // Sets up an image object to be displayed.
            if (MyImage != null)
            {
                MyImage.Dispose();
            }

            // Stretches the image to fit the pictureBox.
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            MyImage = new Bitmap(fileToDisplay);
            pictureBox1.ClientSize = new Size(xSize, ySize);
            pictureBox1.Image = (Image)MyImage;
        }

        //code to display image
        public void ShowMyImage2(String fileToDisplay, int xSize, int ySize)
        {
            // Sets up an image object to be displayed.
            if (MyImage != null)
            {
                MyImage.Dispose();
            }

            // Stretches the image to fit the pictureBox.
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            MyImage2 = new Bitmap(fileToDisplay);
            pictureBox2.ClientSize = new Size(xSize, ySize);
            pictureBox2.Image = (Image)MyImage2;
        }

        public void addItemsToCardClassBox()
        {
            cardClassBox.Items.Add("Melee");
            cardClassBox.Items.Add("Range");
            cardClassBox.Items.Add("Siege");
        }
        public void addItemsToCardClassBox2()
        {
            CreateCardClassBox.Items.Add("Melee");
            CreateCardClassBox.Items.Add("Range");
            CreateCardClassBox.Items.Add("Siege");
        }

        //populate card list combo box in Edit cards tab
        public void addItemsToCardListBox()
        {
            cardsListBox.Items.Clear();
            foreach (card c in myCards)
            {
                cardsListBox.Items.Add(c.cardname);
            }
        }

        //load all images from images folder
        void LoadImages()
        {
            imageListPaths.AddRange(Directory.GetFiles(path + imagePath, "*.png"));
            foreach (string path in imageListPaths)
            {
                imageNames.Add(Path.GetFileName(path));
            }
            foreach (string name in imageNames)
            {
                CreateCardComboBox.Items.Add(name);
                ImagesBox.Items.Add(name);
            }
        }

        //load data from JSON and create all card objects
        void JSONFileCard(string filename)
        {
            myCards.Clear();
            string jsonString = File.ReadAllText(filename);
            
            dynamic jsonObj = JsonConvert.DeserializeObject(jsonString);

            jsonObj = (IEnumerable)jsonObj;
            foreach (var itemsList in jsonObj)
                {
                    card card = new card();
                    card.cardname = itemsList["cardname"];
                    card.name = itemsList["name"]; //pictures name
                    card.score = itemsList["score"];
                    card.type = itemsList["type"];
                    myCards.Add(card);
                }
            
        }

        // save all current card data in "newCards.json file"
        void saveToJSONFile()
        {
            string jsonString = JsonConvert.SerializeObject(myCards, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText("cards.json", jsonString);
        }

        //display image from select image box in Edit cards tab
        private void ImagesBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string imageName = (string)ImagesBox.SelectedItem;
            ShowMyImage(path + imagePath + imageName, 100, 100);
        }

        //display image from selected image in Create new card tab
        private void CreateCardComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string imageName = (string)CreateCardComboBox.SelectedItem;
            ShowMyImage2(path + imagePath + imageName, 100, 100);
        }

        //Select Jsonfile to load cards from.
        private void LoadCardButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = path;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                filePath = openFileDialog.FileName;
            }
            JSONFileCard(filePath);
            addItemsToCardListBox();
            
        }

        //Modify selected card and refresh entire Select card combobox list in Edit tabs
        private void EditCardButton_Click(object sender, EventArgs e)
        {
            card selectedCard = getCard();
            if (selectedCard != null)
            {
                string newName = setCardNameBox.Text;
                selectedCard.cardname = newName;
                string imageName = (string)ImagesBox.SelectedItem;
                selectedCard.name = imageName.Substring(0, imageName.Length - 4);
                selectedCard.score = int.Parse(cardPowerText.Text);
                string type = (string)cardClassBox.Text;
                selectedCard.type = type;
                
                addItemsToCardListBox();
            }
            
        }


        //get card from myCards list when selecting card from Edit tab
        private card getCard()
        {
            string cardName = (string)cardsListBox.SelectedItem;
            if (cardName != "")
            {
                foreach (card c in myCards)
                {
                    if (c.cardname == cardName)
                    {
                        return c;
                    }
                }
                return null;
            }
            else return null;
        }

        //Display image in Create card tab when selected from DropDown Menu
        private void cardsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string cardName = (string)cardsListBox.SelectedItem;
            card currentCard = getCard();
            if (currentCard.cardname == cardName)
                {
                    ShowMyImage(path + imagePath + currentCard.name + ".png", 100, 100);
                    currentPowerBox.Text = currentCard.score.ToString();
                    currentClassBox.Text = currentCard.type;
                    currentCardPic.Text = currentCard.name + ".png";
                }
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void setCardNameBox_TextChanged(object sender, EventArgs e)
        {

        }

        //add card to myCards List
        private void AddCardButton_Click(object sender, EventArgs e)
        {
            card newCard = new card();
            newCard.cardname = EnterCardNameBox.Text;
            string imageName = CreateCardComboBox.SelectedItem.ToString();
            newCard.name = imageName.Substring(0, imageName.Length - 4);
            newCard.type = CreateCardClassBox.SelectedItem.ToString();
            newCard.score = int.Parse(CreateCardSetPowerBox.Text);
            myCards.Add(newCard);
            addItemsToCardListBox();
        }

        //call save function
        private void saveButton1_Click(object sender, EventArgs e)
        {
            saveToJSONFile();
        }
    }
}
